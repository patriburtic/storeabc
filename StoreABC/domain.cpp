#include "domain.h"

const Product& Product::operator=(const Product& ot)
{
	name = ot.name;
	type = ot.type;
	producer = ot.producer;
	price = ot.price;
	return *this;
}

string Product::getName() const
{
	return name;
}

string Product::getType() const
{
	return type;
}

string Product::getProducer() const
{
	return producer;
}

double Product::getPrice() const
{
	return price;
}
