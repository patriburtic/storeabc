#include "GUI.h"
#include <QtWidgets/qmessagebox.h>

void GUI::makeInterface()
{
	bttnsLayout->addWidget(addBttn);
	bttnsLayout->addWidget(modifyBttn);
	bttnsLayout->addWidget(searchBttn);
	bttnsLayout->addWidget(deleteBttn);

	formLayout->addRow("&Name: ", nameTxt);
	formLayout->addRow("&Type: ", typeTxt);
	formLayout->addRow("&Price: ", priceTxt);
	formLayout->addRow("&Producer: ",producerTxt);

	leftLayout->addLayout(formLayout);
	leftLayout->addLayout(bttnsLayout);
	leftLayout->addWidget(exitBttn);

	mainLayout->addLayout(leftLayout);
	mainLayout->addWidget(myList);

	this->setLayout(mainLayout);

}

void GUI::connectAll()
{
	QObject::connect(myList, &QListWidget::itemSelectionChanged, [this]() {
		if (myList->selectedItems().isEmpty()) {
			nameTxt->setText("");
			typeTxt->setText("");
			priceTxt->setText("");
			producerTxt->setText("");
		}
		else {
			auto selItem = myList->selectedItems().at(0);
			auto txt = selItem->text();
			nameTxt->setText(txt);
			auto stringTxt = txt.toStdString();
			auto product = service.findProduct(stringTxt);
			typeTxt->setText(QString::fromStdString(product.getType()));
			priceTxt->setText(QString::number(product.getPrice()));
			producerTxt->setText(QString::fromStdString(product.getProducer()));
		}
		});

	QObject::connect(addBttn, &QPushButton::clicked, [&]() {

		auto name = nameTxt->text().toStdString();
		auto type = typeTxt->text().toStdString();
		auto price = priceTxt->text().toDouble();
		auto producer = producerTxt->text().toStdString();
		try {
			service.addProduct(name, type, price, producer);
			loadData();
		}
		catch (string ex) {
			QMessageBox::warning(nullptr, "ATENTION", QString::fromStdString(ex));
		}
		nameTxt->setText("");
		typeTxt->setText("");
		priceTxt->setText("");
		producerTxt->setText("");

		});

	QObject::connect(modifyBttn, &QPushButton::clicked, [&]() {
		auto name = nameTxt->text().toStdString();
		auto type = typeTxt->text().toStdString();
		auto price = priceTxt->text().toDouble();
		auto producer = producerTxt->text().toStdString();
		try {
			service.modifyProduct(name, type, price, producer);
			loadData();
		}
		catch (string ex) {
			QMessageBox::warning(nullptr, "ATENTION", QString::fromStdString(ex));
		}
		nameTxt->setText("");
		typeTxt->setText("");
		priceTxt->setText("");
		producerTxt->setText("");
		});

	QObject::connect(deleteBttn, &QPushButton::clicked, [&]() {
		auto name = nameTxt->text().toStdString();
		try {
			service.removeProduct(name);
			loadData();
		}
		catch (string ex) {
			QMessageBox::warning(nullptr, "ATENTION", QString::fromStdString(ex));
		}
		nameTxt->setText("");
		typeTxt->setText("");
		priceTxt->setText("");
		producerTxt->setText("");
		});

	QObject::connect(exitBttn, &QPushButton::clicked, [&]() {
		QMessageBox::information(nullptr, "ATENTION", "The app will close");
		close();
		});

	QObject::connect(searchBttn, &QPushButton::clicked, [&]() {
		auto name = nameTxt->text().toStdString();
		try {
			auto product = service.findProduct(name);
			typeTxt->setText(QString::fromStdString(product.getType()));
			priceTxt->setText(QString::number(product.getPrice()));
			producerTxt->setText(QString::fromStdString(product.getProducer()));
		}
		catch (string ex) {
			QMessageBox::warning(nullptr, "ATENTION", QString::fromStdString(ex));
			typeTxt->setText("");
			priceTxt->setText("");
			producerTxt->setText("");
		}
		});
}

void GUI::loadData()
{
	myList->clear();
	for (const auto& product : service.getProducts()) {
		myList->addItem(QString::fromStdString(product.getName()));
	}
}
