#pragma once
#include "repo.h"
class StoreService
{
private:
	StoreRepo& repo;
public:
	StoreService(StoreRepo& re) :repo{ re } {}
	StoreService(const StoreService& s) = delete;
	void addProduct(const string& name, const string& type, const double& price, const string& producer);
	void removeProduct(const string& name);
	void modifyProduct(const string& name, const string& ntype, const double& nprice, const string& nproducer);
	const Product& findProduct(const string& name);
	const vector<Product>& getProducts();
};

