#pragma once
#include <QtWidgets/qpushbutton.h>
#include <QtWidgets/qwidget>
#include <QtWidgets/qboxlayout>
#include <QtWidgets/qformlayout>
#include <QtWidgets/qlineedit>
#include <QtWidgets/qlistwidget>
#include "service.h"
class GUI:public QWidget
{
private:
	StoreService& service;
	QHBoxLayout* mainLayout = new QHBoxLayout;
	QHBoxLayout* bttnsLayout = new QHBoxLayout;
	QVBoxLayout* leftLayout = new QVBoxLayout;
	QFormLayout* formLayout = new QFormLayout;
	QListWidget* myList = new QListWidget;
	QLineEdit* nameTxt = new QLineEdit;
	QLineEdit* typeTxt = new QLineEdit;
	QLineEdit* priceTxt = new QLineEdit;
	QLineEdit* producerTxt = new QLineEdit;
	QPushButton* addBttn = new QPushButton{ "&Add" };
	QPushButton* modifyBttn = new QPushButton{ "&Modify" };
	QPushButton* searchBttn = new QPushButton{ "&Search" };
	QPushButton* deleteBttn = new QPushButton{ "&Delete" };
	QPushButton* exitBttn = new QPushButton{ "&Exit" };
public:
	GUI(StoreService& s) :service{ s } {
		makeInterface();
		loadData();
		connectAll();
		
	}
	void makeInterface();
	void connectAll();
	void loadData();

};

