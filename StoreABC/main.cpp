#include <QtWidgets/QApplication>
#include "repo.h"
#include "service.h"
#include "GUI.h"


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	StoreRepo rep{ "products.txt" };
	StoreService service{ rep };
	GUI w{ service };
	w.show();

	return a.exec();
}
