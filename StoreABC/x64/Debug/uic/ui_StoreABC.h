/********************************************************************************
** Form generated from reading UI file 'StoreABC.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STOREABC_H
#define UI_STOREABC_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StoreABCClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *StoreABCClass)
    {
        if (StoreABCClass->objectName().isEmpty())
            StoreABCClass->setObjectName(QString::fromUtf8("StoreABCClass"));
        StoreABCClass->resize(600, 400);
        menuBar = new QMenuBar(StoreABCClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        StoreABCClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(StoreABCClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        StoreABCClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(StoreABCClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        StoreABCClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(StoreABCClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        StoreABCClass->setStatusBar(statusBar);

        retranslateUi(StoreABCClass);

        QMetaObject::connectSlotsByName(StoreABCClass);
    } // setupUi

    void retranslateUi(QMainWindow *StoreABCClass)
    {
        StoreABCClass->setWindowTitle(QCoreApplication::translate("StoreABCClass", "StoreABC", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StoreABCClass: public Ui_StoreABCClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STOREABC_H
