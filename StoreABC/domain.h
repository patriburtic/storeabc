#pragma once
#include <string>
using namespace std;
class Product{
private:
	string name, type, producer;
	double price;
public:
	Product(const string& n, const string& t, const double& p, const string& pr) :name{ n }, type{ t }, price{ p }, producer{ pr }{}
	Product(const Product& ot) :name{ ot.name }, type{ ot.type }, price{ ot.price }, producer{ ot.producer }{}
	const Product& operator=(const Product& ot);
	string getName() const;
	string getType() const;
	string getProducer() const;
	double getPrice() const;

};

