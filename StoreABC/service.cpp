#include "service.h"

void StoreService::addProduct(const string& name, const string& type, const double& price, const string& producer)
{
	Product p(name, type, price, producer);
	repo.add(p);
}

void StoreService::removeProduct(const string& name)
{
	auto p = repo.find(name);
	repo.remove(p);
}

void StoreService::modifyProduct(const string& name, const string& ntype, const double& nprice, const string& nproducer)
{
	Product np(name, ntype, nprice, nproducer);
	repo.modify(name, np);
}

const Product& StoreService::findProduct(const string& name)
{
	return repo.find(name);
}

const vector<Product>& StoreService::getProducts()
{
	return repo.getAll();
}
