#pragma once
#include "domain.h"
#include <vector>

using namespace std;
class StoreRepo{
private:
	vector<Product> allProducts;
	string filename;
	void loadFromFile();
	void writeToFile();
public:
	StoreRepo(string fn) :filename{ fn } {
		loadFromFile();
	}
	StoreRepo(const StoreRepo& rep) = delete;
	const bool exist(const Product& p);
	const Product& find(const string name);
	void add(const Product& p);
	void remove(const Product& p);
	void modify(const string& name, const Product& new_p);
	const vector<Product>& getAll();
};

