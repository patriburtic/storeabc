#include "repo.h"
#include <fstream>
void StoreRepo::loadFromFile()
{
	ifstream f(filename);
	if (!f.is_open())
		throw string{ "Error open: " + filename };
	else {
		while (!f.eof()) {
			string name, type, producer;
			double price;
			f >> name;
			f >> type;
			f >> price;
			f >> producer;
			if (f.eof())break;
			Product p(name, type, price, producer);
			add(p);
		}
		f.close();
	}
}

void StoreRepo::writeToFile()
{
	ofstream f(filename);
	if (!f.is_open())
		throw string{ "Error open: " + filename };
	else {
		for (auto& p : allProducts) {
			f << p.getName();
			f << endl;
			f << p.getType();
			f << endl;
			f << p.getPrice();
			f << endl;
			f << p.getProducer();
			f << endl;
		}
	}
	f.close();
}

const bool StoreRepo::exist(const Product& p)
{
	try {
		auto product = find(p.getName());
		return true;
	}
	catch (...) {
		return false;
	}
}

const Product& StoreRepo::find(const string name)
{
	auto p_it = find_if(allProducts.begin(), allProducts.end(), [name](Product& p) {return p.getName() == name; });
	if (p_it == allProducts.end())
		throw string{ "This product doesn't exist!\n" };
	else
		return *p_it;
}

void StoreRepo::add(const Product& p)
{
	if (exist(p)) {
		throw string{ "This product already exists!\n" };
	}
	allProducts.push_back(p);
	writeToFile();
}

void StoreRepo::remove(const Product& p)
{
	bool found = false;
	auto p_it = find_if(allProducts.begin(), allProducts.end(), [p](Product& pr) {return p.getName() == pr.getName(); });
	if (p_it != allProducts.end()) {
		found = true;
		allProducts.erase(p_it);
	}
	if (!found)
		throw string{ "This product doesn't exist!" };
}

void StoreRepo::modify(const string& name, const Product& new_p)
{
	bool found = false;
	for (auto& product : allProducts) {
		if (product.getName() == name) {
			found = true;
			product = new_p;
			break;
		}
	}
	if (!found) {
		throw string{ "This offer doesn't exist!" };
	}
}

const vector<Product>& StoreRepo::getAll()
{
	return allProducts;
}
